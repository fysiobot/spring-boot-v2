
package fysiobotv2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author markusm
 */
@Controller
public class FysiobotController {
    
    @GetMapping("/")
    public String home() {
        return "index";
    }
    @GetMapping("/signup")
    public String signup() {
        return "signup";
    }
    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
