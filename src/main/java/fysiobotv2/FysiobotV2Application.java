package fysiobotv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FysiobotV2Application {

	public static void main(String[] args) {
		SpringApplication.run(FysiobotV2Application.class, args);
	}

}
